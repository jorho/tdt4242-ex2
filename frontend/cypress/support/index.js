// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:

import './commands'

var coachUser = {
    username: null,
    password: null,
    email: 'email@email.com',
    phone_number: '00000000',
    street_address: 'Kong inges gt 22',
    city: 'Trondheim',
    country: 'Norway',
}
var athleteUser = {
    username: null,
    password: null,
    email: 'email@email.com',
    phone_number: '00000000',
    street_address: 'Kong inges gt 22',
    city: 'Trondheim',
    country: 'Norway',
}
var apiUrl = "localhost:8000"
var baseUrl = "localhost:9090"

export function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

function generateUser() {
    return {
        username: `test_user_${makeid(10)}`,
        password: makeid(10),
    }
}

function registerUser(user) {
    cy.visit(`${baseUrl}/index.html`)
    cy.contains('Welcome to SecFit!')
    cy.get('#btn-register').click()

    cy.url().should('include', 'register.html')

    cy.get('input[name="username"]').type(user.username)

    cy.get('input[name="password"]').type(user.password)
    cy.get('input[name="password1"]').type(user.password)

    cy.get('#btn-create-account').click()
    cy.url().should('include', 'workouts.html')
    logout()

}

function login(user) {
    cy.visit(`${baseUrl}/index.html`)
    cy.contains('Welcome to SecFit!')
    cy.get('#btn-login-nav').click()

    cy.url().should('include', 'login.html')

    cy.get('input[name="username"]').type(user.username)

    cy.get('input[name="password"]').type(user.password)

    cy.intercept({method: 'GET', url: '/workouts.html' }).as('loginRequest')
    cy.get('#btn-login').click()
    cy.wait('@loginRequest')
}

function logout() {
    cy.get('#btn-logout', { timeout: 10000 }).click()
    cy.url().should('include', 'index.html')
}


before(() => {
    coachUser = {
        ...coachUser,
        ...generateUser()
    }
    athleteUser = {
        ...athleteUser,
        ...generateUser()
    }
    registerUser(coachUser)
    registerUser(athleteUser)
    cy.visit(`${baseUrl}/index.html`)
})

beforeEach(() => {
    cy.wrap(coachUser).as('coachUser')
    cy.wrap(athleteUser).as('athleteUser')
    cy.wrap(login).as('login')
})
