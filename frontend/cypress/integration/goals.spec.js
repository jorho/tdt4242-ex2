const getDate = () => {
    let d = new Date();
    const offset = d.getTimezoneOffset();
    d = new Date(d.getTime() - (offset*60*1000));
    return d.toISOString().split('T')[0];
}

//

describe('Check goals', () => {
    it('Create goal', function() {
        this.login(this.athleteUser)
        cy.get('#nav-goals').click()
        cy.get('#btn-create-goal').click()
        cy.url().should('include', 'goal.html')

        cy.get('#inputName', { timeout: 10000 }).type("GOAL_NAME")
        cy.get('#inputDescription').type("GOAL_DESCRIPTION")

        cy.get('#inputDateTime')
            .click()
            .then(input => {
                input[0].dispatchEvent(new Event('input', { bubbles: true }))
                input.val('2017-04-30T13:00')
            })
            .click()
       // cy.intercept({method: 'POST', url: '/api/goal/' }).as('createRequest')
        cy.get('#btn-ok-goal').click()
        cy.wait(1000)

        cy.get('h2').eq(0).contains("GOAL_NAME")
        //cy.wait('@createRequest')
        cy.url().should('include', 'goals.html')
    })

    it('Edit goal', function() {
        this.login(this.athleteUser)
        cy.get('#nav-goals').click()
        cy.get('.card-link')
            .eq(0).click()

        cy.get('#inputName', { timeout: 10000 }).type("_EDITED")



        //cy.intercept({method: 'DELETE', url: '/api/goal/1/' }).as('createRequest')
        cy.get('#btn-ok-goal').click()
        cy.wait(1000)

        cy.get('h2').eq(0).contains("GOAL_NAME_EDITED")

    })


    it('Delete goal', function() {
        this.login(this.athleteUser)
        cy.get('#nav-goals').click()
        cy.get('.card-link')
            .eq(0).click()

        //cy.intercept({method: 'GET', url: '/api/goal/1/' }).as('createRequest')
        //cy.wait('@createRequest')
        cy.wait(1000)


        //cy.intercept({method: 'DELETE', url: '/api/goal/1/' }).as('createRequest')
        cy.get('#btn-delete-goal').click()
        cy.wait(1000)
        //cy.wait('@createRequest')

        cy.url().should('include', 'goals.html')
    })
})
