describe('Create exercise', () => {
    it('Exercise creation', function() {
        this.login(this.athleteUser)
        cy.get('#nav-exercises').click()
        cy.get('#btn-create-exercise').click()
        cy.url().should('include', 'exercise.html')

        cy.get('#inputName', { timeout: 10000 }).type('Workout')
        cy.get('#inputDescription').type('Description')
        cy.get('#inputUnit').type('10')
        cy.get('#inputDuration').type("-10")
        cy.get('#inputCalories').type("-10")
        cy.get('select[name="muscleGroup"]').eq(0).select('Legs', {force: true})

        cy.intercept({method: 'POST', url: '/api/exercises/' }).as('createRequest')
        cy.get('#btn-ok-exercise').click()
        cy.wait('@createRequest')

        cy.get('.alert').should('be.visible');
    })

})
