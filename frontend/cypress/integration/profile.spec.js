

describe('Test profile page', () => {

    it('Edit profile', function() {
        this.login(this.athleteUser)

        cy.get('#nav-profile').click()
        cy.get('#main_gym').type("SiT Moholt")
        cy.get('#favourite_exercise').type("Cardio")
        cy.get('#btn-edit-account').click()
        cy.wait(1000)
        cy.url().should('include', 'profile.html')

        cy.get('#main_gym').should('have.value', 'SiT Moholt');
        cy.get('#favourite_exercise').should('have.value', 'Cardio');
    })


    it('Connect to coach and see if the coach is added in profile', function() {
        this.login(this.athleteUser)

        cy.get('#nav-mycoach').click()
        cy.get('#input-coach').type(`${this.coachUser.username}`, {force: true})

        cy.get('#button-edit-coach').click()
        cy.get('#button-set-coach').click({force: true})
        cy.wait(2000)

        cy.get('#nav-profile').click()
        cy.url().should('include', 'profile.html')
        cy.get('#coach-content').contains(`${this.coachUser.username}`)
    })

})
