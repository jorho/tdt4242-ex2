var baseUrl = "localhost:9090"

describe('Black box testing', () => {
    function logWorkout(workoutname, publicbool){
        cy.get("#btn-create-workout").click();
        cy.get('input[name="name"]').type(workoutname);
        if(publicbool){
            cy.get("#inputVisibility").select("Public");
        }else{
            cy.get("#inputVisibility").select("Private")
        }
        cy.get('input[name="date"]').click().then(input => {
            input[0].dispatchEvent(new Event('input', { bubbles: true }))
            input.val('2017-04-30T13:00')
        }).click()
        cy.get('#inputNotes').type('This is a note')
        cy.get("#btn-ok-workout").click();
    }


    it('Logg personal workout and check if added in my list', function() {
        this.login(this.athleteUser)
        cy.get('#list-my-workouts-list').click();
        cy.get('h5').contains('Public Workout');
    })

    it('Logg public personal and check if added', function() {
        this.login(this.athleteUser)
        cy.get('#list-public-workouts-list').click();
        cy.get('h5').contains('Public Workout');
    })

    it('Logg public and check if added', function() {
        this.login(this.athleteUser)
        logWorkout("Public Workout", true);
        cy.url().should('include', "/workouts.html");
        cy.get('h5').contains('Public Workout')
    })


    it('See details of public workout', function() {
        this.login(this.athleteUser)
        cy.get('#list-public-workouts-list').click();
        cy.get('h5').contains('Public Workout').parent().click();
        cy.get('#inputName').should('be.visible');
        cy.get('#inputNotes').should('be.visible');
        cy.get('#customFile').should('be.visible');
    })

    it('User should not be able to see other private workouts', function() {
        this.login(this.athleteUser)
        logWorkout("Private Workout", false);
        cy.get('#btn-logout', { timeout: 10000 }).click()
        cy.wait(4000)
        this.login(this.coachUser)
        cy.wait(2000)
        cy.get('h5').contains('Private Workout').should('not.exist');
        cy.get('#list-public-workouts-list').click();
        cy.get('h5').contains('Private Workout').should('not.exist');
    })

})
