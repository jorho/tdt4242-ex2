
const randomUsername = () => {
    return Math.random().toString(36).substring(7);
}
var baseUrl = "localhost:9090"
/**
 * The only validation done in the frontend is thorugh email
 */
const HundredAndFiftyTwo = "tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 tdt4242 ";

describe('boundary tests registration page', () => {
    beforeEach(() => {
        cy.visit(`${baseUrl}/register.html`)
    });

    it('Test lower boundary limit', () => {
        cy.get('#btn-create-account').click()

        cy.get('.alert').should('be.visible');
        cy.get('.alert').get('li').contains('username').children('ul').children('li').contains('This field may not be blank.')
        cy.get('li').contains('password').children('ul').children('li').contains('This field may not be blank.')
        cy.get('li').contains('password1').children('ul').children('li').contains('This field may not be blank.')

    });

    it('Test upper boundary limit', () => {

        cy.get('input[name="username"]').type(HundredAndFiftyTwo)
        cy.get('input[name="email"]').type(HundredAndFiftyTwo)
        cy.get('input[name="password"]').type(HundredAndFiftyTwo)
        cy.get('input[name="password1"]').type(HundredAndFiftyTwo)
        cy.get('input[name="phone_number"]').type(HundredAndFiftyTwo)
        cy.get('input[name="country"]').type(HundredAndFiftyTwo)
        cy.get('input[name="city"]').type(HundredAndFiftyTwo)
        cy.get('input[name="street_address"]').type(HundredAndFiftyTwo)
        cy.get('input[name="main_gym"]').type(HundredAndFiftyTwo)
        cy.get('input[name="favourite_exercise"]').type(HundredAndFiftyTwo)


        cy.get('#btn-create-account').click()
        cy.get('.alert').should('be.visible');
        cy.get('li').contains('Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters.')
        cy.get('li').contains('Ensure this field has no more than 150 characters.')
        cy.get('li').contains('Enter a valid email address.')
        cy.get('li').contains('phone_number').children('ul').children('li').contains("Ensure this field has no more than 50 characters.")
        cy.get('li').contains('country').children('ul').children('li').contains("Ensure this field has no more than 50 characters.")
        cy.get('li').contains('city').children('ul').children('li').contains("Ensure this field has no more than 50 characters.")
        cy.get('li').contains('street_address').children('ul').children('li').contains("Ensure this field has no more than 50 characters.")
        cy.get('li').contains('main_gym').children('ul').children('li').contains("Ensure this field has no more than 50 characters.")
        cy.get('li').contains('favourite_exercise').children('ul').children('li').contains("Ensure this field has no more than 50 characters.")

    });



});

