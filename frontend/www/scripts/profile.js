

async function init() {
    const username = sessionStorage.getItem("username");
    let userResponse = await sendRequest("GET", `${HOST}/api/users/${username}/`);
    await userResponse.json().then(async (val) => {
        if(val){
            console.log(val);
            document.getElementById("username").value = `${val.username}`;
            document.getElementById("email").value = `${val.email}`;
            document.getElementById("phone_number").value = `${val.phone_number}`;
            document.getElementById("country").value = `${val.country}`;
            document.getElementById("city").value = `${val.city}`;
            document.getElementById("street_address").value = `${val.street_address}`;
            document.getElementById("main_gym").value = `${val.main_gym}`;
            document.getElementById("favourite_exercise").value = `${val.favourite_exercise}`;

            let athletes = val.athletes;
            let athleteContainer = document.getElementById('athlete-content');
            let athleteTemplate = document.querySelector("#template-exercise");
            athletes.forEach(async (athleteUrl) => {
                let athleteResponse = await sendRequest("GET", `${athleteUrl}`);
                await athleteResponse.json().then((athlete) => {
                    if(athlete){
                        console.log(athlete.username);
                        const athleteAnchor = athleteTemplate.content.firstElementChild.cloneNode(true);

                        const h5 = athleteAnchor.querySelector("h5");
                        h5.textContent = athlete.username;

                        const p = athleteAnchor.querySelector("p");
                        p.textContent = athlete.phone_number;

                        athleteContainer.appendChild(athleteAnchor);

                    }
                }).catch(console.log);
            });


            let coach = val.coach;
            let coachContainer = document.getElementById('coach-content');
            let coachTemplate = document.querySelector("#template-coach");
            let coachResponse = await sendRequest("GET", `${coach}`);
            await coachResponse.json().then((val) => {
                if(val){
                    const coachAnchor = coachTemplate.content.firstElementChild.cloneNode(true);

                    const h5 = coachAnchor.querySelector("h5");
                    h5.textContent = val.username;

                    const p = coachAnchor.querySelector("p");
                    p.textContent = val.phone_number;

                    coachContainer.appendChild(coachAnchor);

                }
            });
        }

    }).catch(console.log);



}

async function updateUser(event) {
    const usrName = sessionStorage.getItem("username");
    let form = document.querySelector("#form-edit-user");
    let formData = new FormData(form);

    let body = {
        username: usrName,
        email: formData.get("email"),
        phone_number: formData.get("phone_number"),
        country: formData.get("country"),
        city: formData.get("city"),
        street_address: formData.get("street_address"),
        main_gym: formData.get("main_gym"),
        favourite_exercise: formData.get("favourite_exercise")
    };

    const username = sessionStorage.getItem("username");
    response = await sendRequest("POST", `${HOST}/api/users/${username}/`, body);
    if (response.ok) {
        console.log("UPDATED PROFILE");
    } else {
        console.log("CAN'T GET JWT TOKEN ON REGISTRATION");
        let data = await response.json();
        let alert = createAlert("Profile update could not complete. Try again!", data);
        document.body.prepend(alert);
    }



    /*
    let response = await sendRequest("POST", `${HOST}/api/users/`, formData, "");

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Registration failed!", data);
        document.body.prepend(alert);

    } else {
        let body = {
            username:       formData.get("username"),
            password:       formData.get("password"),
            phone_number:   formData.get("phone_number"),
            country:        formData.get("country"),
            city:           formData.get("city"),
            street_address: formData.get("street_address"),
            main_gym:   formData.get("main_gym"),
            favourite_exercise:   formData.get("favourite_exercise")
        };
        response = await sendRequest("POST", `${HOST}/api/token/`, body);
        if (response.ok) {
            let data = await response.json();
            setCookie("access", data.access, 86400, "/");
            setCookie("refresh", data.refresh, 86400, "/");
            sessionStorage.setItem("username", formData.get("username"));
        } else {
            console.log("CAN'T GET JWT TOKEN ON REGISTRATION");
            let data = await response.json();
            let alert = createAlert("Registration could not complete. Try again!", data);
            document.body.prepend(alert);
        }
        form.reset();
        //window.location.replace("workouts.html");
    }
     */
}

document.querySelector("#btn-edit-account").addEventListener("click", async (event) => await updateUser(event));
init();
