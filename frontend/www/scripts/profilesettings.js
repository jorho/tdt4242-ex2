let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;

//TODO change variablenames and check
class gym {
    constructor(type) {
        this.isValidType = false;
        this.validTypes = ["SIT", "IMPULS", "3T", "STAMINA"];

        this.type = this.validTypes.includes(type) ? type : undefined;
    };

    setMuscleGroupType = (newType) => {
        this.isValidType = false;

        if(this.validTypes.includes(newType)){
            this.isValidType = true;
            this.type = newType;
        }
        else{
            alert("Invalid gym!");
        }

    };

    getMuscleGroupType = () => {
        console.log(this.type, "SWIOEFIWEUFH")
        return this.type;
    }
}

function handleCancelButtonDuringEdit() {
    console.log("fjpfjhpisdfhøidksfho")
    /*
    setReadOnly(true, "#form-exercise");
    document.querySelector("select").setAttribute("disabled", "")
    okButton.className += " hide";
    deleteButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-exercise");
    if (oldFormData.has("name")) form.name.value = oldFormData.get("name");
    if (oldFormData.has("favouriteExercise")) form.name.value = oldFormData.get("favouriteExercise");
    if (oldFormData.has("gymGroup")) form.muscleGroup.value = oldFormData.get("gymGroup");

    oldFormData.delete("name");
    oldFormData.delete("favouriteExercise");
    oldFormData.delete("gymGroup");
     */

}

function handleCancelButtonDuringCreate() {
    window.location.replace("profile.html");
}


 async function createExercise() {
    document.querySelector("select").removeAttribute("disabled")
    console.log("dasdadasda")
    let form = document.querySelector("#gym");
    let formData = new FormData(form);
    let body = {"name": formData.get("name"),
        "description": formData.get("description"),
        "duration": formData.get("duration"),
        "calories": formData.get("calories"),
        "muscleGroup": formData.get("gym"),
        "unit": formData.get("unit")};

    let response = await sendRequest("POST", `${HOST}/api/exercises/`, body);

    if (response.ok) {
        window.location.replace("exercises.html");
    } else {
        let data = await response.json();
        let alert = createAlert("Coulds not create new exercise!", data);
        document.body.prepend(alert);
    }

    console.log("TEST*IJG")
} 

function handleEditExerciseButtonClick() {
    setReadOnly(false, "#form-exercise");

    document.querySelector("select").removeAttribute("disabled")

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");
    deleteButton.className = deleteButton.className.replace(" hide", "");

    cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-exercise");
    oldFormData = new FormData(form);
}

async function deleteExercise(id) {
    let response = await sendRequest("DELETE", `${HOST}/api/exercises/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace("exercises.html");
    }
}

//Testing
async function retrieveGym(id) {
    let response = await sendRequest("GET", `${HOST}/api/users/${id}/`);

    console.log(response.ok)

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve exercise data!", data);
        document.body.prepend(alert);
    } else {
        document.querySelector("select").removeAttribute("disabled")
        let exerciseData = await response.json();
        let form = document.querySelector("#gym");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector
            key !== "gym" ? selector = `input[name="${key}"], textarea[name="${key}"]` : selector = `select[name=${key}]`
            let input = form.querySelector(selector);
            let newVal = exerciseData[key];
            input.value = newVal;
        }
        document.querySelector("select").setAttribute("disabled", "")
    }
}

async function updateGym(id) {
    let form = document.querySelector("#gym");
    let formData = new FormData(form);

    let muscleGroupSelector = document.querySelector("select")
    muscleGroupSelector.removeAttribute("disabled")

    let selectedGym = new gym(formData.get("gym"));

    let body = {
        "gym": selectedGym.getMuscleGroupType(),};
    let response = await sendRequest("PUT", `${HOST}/api/users/${id}/`, body);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not update gym ${id}`, data);
        document.body.prepend(alert);
    } else {
        console.log(response);
        window.location.replace("exercises.html");
   
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    cancelButton = document.querySelector("#btn-cancel-exercise");
    okButton = document.querySelector("#btn-ok-exercise");
    deleteButton = document.querySelector("#btn-delete-exercise");
    editButton = document.querySelector("#btn-edit-exercise");
    oldFormData = null;

    const urlParams = new URLSearchParams(window.location.search);

    // view/edit
    if (urlParams.has('id')) {
        const exerciseId = urlParams.get('id');
        await retrieveExercise(exerciseId);

        editButton.addEventListener("click", handleEditExerciseButtonClick);
        deleteButton.addEventListener("click", (async (id) => await deleteExercise(id)).bind(undefined, exerciseId));
        okButton.addEventListener("click", (async (id) => await updateGym(id)).bind(undefined, exerciseId));
    }
    //create
    else {
        setReadOnly(false, "#form-exercise");

        editButton.className += " hide";
        okButton.className = okButton.className.replace(" hide", "");
        cancelButton.className = cancelButton.className.replace(" hide", "");

        okButton.addEventListener("click", async () => await updateGym());
        cancelButton.addEventListener("click", handleCancelButtonDuringCreate);
    }
});
