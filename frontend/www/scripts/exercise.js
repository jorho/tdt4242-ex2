let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;

const NAME = "name";
const DESCRIPTION = "description";
const DURATION = "duration";
const CALORIES = "calories";
const MUSCLE_GROUP = "muscleGroup";
const UNIT = "unit";

const validTypes = ["Legs", "Chest", "Back", "Arms", "Abdomen", "Shoulders"];

const deleteFormData = () => {
    oldFormData.delete(NAME);
    oldFormData.delete(DESCRIPTION);
    oldFormData.delete(DURATION);
    oldFormData.delete(CALORIES);
    oldFormData.delete(MUSCLE_GROUP);
    oldFormData.delete(UNIT);
}

function handleCancelButtonDuringEdit() {
    setReadOnly(true, "#form-exercise");
    document.querySelector("select").setAttribute("disabled", "")
    okButton.className += " hide";
    deleteButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

    const form = document.querySelector("#form-exercise");
    if (oldFormData.has(NAME)) form.name.value = oldFormData.get(NAME);
    if (oldFormData.has(DESCRIPTION)) form.description.value = oldFormData.get(DESCRIPTION);
    if (oldFormData.has(DURATION)) form.duration.value = oldFormData.get(DURATION);
    if (oldFormData.has(CALORIES)) form.calories.value = oldFormData.get(CALORIES);
    if (oldFormData.has(MUSCLE_GROUP)) form.muscleGroup.value = oldFormData.get(MUSCLE_GROUP);
    if (oldFormData.has(UNIT)) form.unit.value = oldFormData.get(UNIT);

    deleteFormData();

}

function handleCancelButtonDuringCreate() {
    window.location.replace("exercises.html");
}

async function createExercise() {
    document.querySelector("select").removeAttribute("disabled")
    const form = document.querySelector("#form-exercise");
    const formData = new FormData(form);
    const body = {name: formData.get(NAME),
        description: formData.get(DESCRIPTION),
        duration: formData.get(DURATION),
        calories: formData.get(CALORIES),
        muscleGroup: formData.get(MUSCLE_GROUP),
        unit: formData.get(UNIT)};

    const response = await sendRequest("POST", `${HOST}/api/exercises/`, body);

    if (response.ok) {
        window.location.replace("exercises.html");
        return;
    }
    const data = await response.json();
    const alert = createAlert("Could not create new exercise!", data);
    document.body.prepend(alert);
}

function handleEditExerciseButtonClick() {
    setReadOnly(false, "#form-exercise");

    document.querySelector("select").removeAttribute("disabled")

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");
    deleteButton.className = deleteButton.className.replace(" hide", "");

    cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

    const form = document.querySelector("#form-exercise");
    oldFormData = new FormData(form);
}

async function deleteExercise(id) {
    const response = await sendRequest("DELETE", `${HOST}/api/exercises/${id}/`);
    if (!response.ok) {
        const data = await response.json();
        const alert = createAlert(`Could not delete exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace("exercises.html");
    }
}

async function retrieveExercise(id) {
    const response = await sendRequest("GET", `${HOST}/api/exercises/${id}/`);

    console.log(response.ok)

    if (!response.ok) {
        const data = await response.json();
        const alert = createAlert("Could not retrieve exercise data!", data);
        document.body.prepend(alert);
        return;
    }
    document.querySelector("select").removeAttribute("disabled")
    const exerciseData = await response.json();
    const form = document.querySelector("#form-exercise");
    const formData = new FormData(form);

    for (const key of formData.keys()) {
        const selector = key !== "muscleGroup" ? `input[name="${key}"], textarea[name="${key}"]` : `select[name=${key}]`
        const input = form.querySelector(selector);
        const newVal = exerciseData[key];
        input.value = newVal;
    }
    document.querySelector("select").setAttribute("disabled", "")
}

const getMuscleGroupType = (type) => {
    return validTypes.includes(type) ? type : undefined;
}

async function updateExercise(id) {
    const form = document.querySelector("#form-exercise");
    const formData = new FormData(form);

    const muscleGroupSelector = document.querySelector("select")
    muscleGroupSelector.removeAttribute("disabled")

    const body = {name: formData.get(NAME),
        description: formData.get(DESCRIPTION),
        duration: formData.get(DURATION),
        calories: formData.get(CALORIES),
        muscleGroup: getMuscleGroupType(formData.get(MUSCLE_GROUP)),
        unit: formData.get(UNIT)};
    const response = await sendRequest("PUT", `${HOST}/api/exercises/${id}/`, body);

    if (!response.ok) {
        const data = await response.json();
        const alert = createAlert(`Could not update exercise ${id}`, data);
        document.body.prepend(alert);
        return;
    }
    muscleGroupSelector.setAttribute("disabled", "")
    // duplicate code from handleCancelButtonDuringEdit
    // you should refactor this
    setReadOnly(true, "#form-exercise");
    okButton.className += " hide";
    deleteButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

    deleteFormData();
}

window.addEventListener("DOMContentLoaded", async () => {
    cancelButton = document.querySelector("#btn-cancel-exercise");
    okButton = document.querySelector("#btn-ok-exercise");
    deleteButton = document.querySelector("#btn-delete-exercise");
    editButton = document.querySelector("#btn-edit-exercise");
    oldFormData = null;

    const urlParams = new URLSearchParams(window.location.search);

    // view/edit
    if (urlParams.has('id')) {
        const exerciseId = urlParams.get('id');
        await retrieveExercise(exerciseId);

        editButton.addEventListener("click", handleEditExerciseButtonClick);
        deleteButton.addEventListener("click", (async (id) => deleteExercise(id)).bind(undefined, exerciseId));
        okButton.addEventListener("click", (async (id) => updateExercise(id)).bind(undefined, exerciseId));
        return;
    }
    //create
    setReadOnly(false, "#form-exercise");

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");

    okButton.addEventListener("click", async () => createExercise());
    cancelButton.addEventListener("click", handleCancelButtonDuringCreate);
});
