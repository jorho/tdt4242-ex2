"""
Tests for the workouts application.
Sources:
https://docs.djangoproject.com/en/4.0/topics/testing/overview/
https://docs.djangoproject.com/en/4.0/topics/testing/tools/
https://docs.djangoproject.com/en/4.0/topics/testing/advanced/
"""
from urllib.request import Request
from django.test import TestCase, Client, RequestFactory

from django.urls import reverse, resolve

#from workouts.views import Workout, WorkoutDetail, WorkoutList
from users.models import User
from workouts.models import Workout, Exercise, ExerciseInstance
from workouts.permissions import IsOwner, IsOwnerOfWorkout, IsCoachAndVisibleToCoach, IsCoachOfWorkoutAndVisibleToCoach, IsPublic, IsWorkoutPublic, IsReadOnly

# Create your tests here.

class IsOwnerTestCase(TestCase):
    def setUp(self):
        #User setup
        self.user_one = User.objects.create(username = "jorsi", phone_number="12345678", country="Norway", 
        city="Trondheim", street_address="street 3", favourite_exercise="Biceps", main_gym="SIT")
        self.user_one.save()

        #Workout setup
        self.workout_one = Workout.objects.create(name="Curl", date="2022-03-03", notes="nice", 
        owner=self.user_one, visibility="PU")

        #Factory setup
        self.factory= RequestFactory()

    def test_is_owner(self):
        test_request = self.factory.get("/") 
        test_request.user = self.user_one
        workout = self.workout_one
        is_owner = IsOwner().has_object_permission(test_request, None, workout)
        self.assertTrue(is_owner)

class IsOwnerOfWorkoutTestCase(TestCase):
    """One workout is associated with 1 or more exercises (instances)
    Each exercise instance must have an exercise type
    So: Create Workout, then exercise, then instance of ex and associated with workout
    User1 owns: workout1, 
    Coach1 Coaches: User1
    ExerciseInstance1 associated with workout1, and is instance of exercise_one
    """
    def setUp(self):
        #Initiate User, Workout, Exercise, ExerciseInstance and RequestFactory
        self.user_one = User.objects.create(username = "jorsi", phone_number="12345678", country="Norway", 
        city="Trondheim", street_address="street 3", favourite_exercise="Biceps", main_gym="SIT")

        self.workout_one = Workout.objects.create(name="Chest", date="2022-03-03", notes="nice", 
        owner=self.user_one, visibility="PU")

        self.exercise_one = Exercise.objects.create(name="Pushups", description="push from ground", duration=30,
        calories=20, muscleGroup="Chest", unit="Amount")

        self.ex_instance_one = ExerciseInstance.objects.create(workout=self.workout_one, exercise=self.exercise_one, 
        sets=3, number=10)
        
        self.factory= RequestFactory()
        
    def test_has_object_permission(self):
        #takes self, request, view obj.
        test_request = self.factory.get("/")
        test_request.user = self.user_one
        ex_instance = self.ex_instance_one
        is_owner = IsOwnerOfWorkout().has_object_permission(test_request, None, ex_instance)
        self.assertTrue(is_owner)

    """
    Has permissions has several outcomes:
    Something else than POST req.
    Post without workout false
     Posting returns true
    """
    def test_has_permission(self):
        #takes self, request, view.
        test_request = self.factory.get("/")
        test_request.user = self.user_one
        test_request.data = {"workout" : "/api/workouts/1/"}
        is_owner= IsOwnerOfWorkout().has_permission(test_request, None)
        self.assertTrue(is_owner)
    
    def test_has_permission_without_workout(self):
        #takes self, request, view.
        test_request = self.factory.post("/")
        test_request.user = self.user_one
        test_request.data = { "workout" : ""} 
        is_owner= IsOwnerOfWorkout().has_permission(test_request, None)
        self.assertFalse(is_owner)
    
    def test_has_permission_post(self):
        #takes self, request, view.
        test_request = self.factory.post("/")
        test_request.user = self.user_one
        test_request.data = {"workout" : "/api/workouts/1/"}
        is_owner = IsOwnerOfWorkout().has_permission(test_request, None)
        self.assertTrue(is_owner)


class IsCoachAndVisibleToCoachTestCase(TestCase):
    """
    Coach1 Coaches: User1, owns workout1
    """
    def setUp(self):
        #Initiate User, Workout, Coach and RequestFactory
        self.user_one = User.objects.create(username = "jorsi", phone_number="12345678", country="Norway", 
        city="Trondheim", street_address="street 3", favourite_exercise="Biceps", main_gym="SIT")
        
        self.coach_one = User.objects.create(username = "sijor", phone_number="23456789", country="Norway", 
        city="Trondheim", street_address="street 4", favourite_exercise="Biceps", main_gym="SIT", coach=self.user_one)

        self.workout_one = Workout.objects.create(name="Chest", date="2022-03-03", notes="nice", 
        owner=self.coach_one, visibility="PU")
        
        self.factory= RequestFactory()

    def test_has_object_permission(self):
        test_request = self.factory.get("/")
        test_request.user = self.user_one
        workout = self.workout_one
        is_coach = IsCoachAndVisibleToCoach().has_object_permission(test_request, None, workout)
        self.assertTrue(is_coach)

class IsCoachOfWorkoutAndVisibleToCoachTestCase(TestCase):
    """
    Coach1 Coaches User1,  & owns workout1
   ExerciseInstance1 associated with workout1, and is instance of exercise_one
    """
    def setUp(self):
        #Initiate User, Coach, Workout, Exercise, ExerciseInstance and RequestFactory
        self.user_one = User.objects.create(username = "jorsi", phone_number="12345678", country="Norway", 
        city="Trondheim", street_address="street 3", favourite_exercise="Biceps", main_gym="SIT")

        self.coach_one = User.objects.create(username = "sijor", phone_number="23456789", country="Norway", 
        city="Trondheim", street_address="street 4", favourite_exercise="Biceps", main_gym="SIT", coach=self.user_one)

        self.workout_one = Workout.objects.create(name="Chest", date="2022-03-03", notes="nice", 
        owner=self.coach_one, visibility="PU")
        
        self.exercise_one = Exercise.objects.create(name="Pushups", description="push from ground", duration=30,
        calories=20, muscleGroup="Chest", unit="Amount")

        self.exercise_instance_one = ExerciseInstance.objects.create(workout=self.workout_one, exercise=self.exercise_one, 
        sets=3, number=10)
        
        self.factory= RequestFactory()

    def test_has_object_permission(self):
        test_request = self.factory.get("/")
        test_request.user = self.user_one
        ins = self.exercise_instance_one

        is_coach = IsCoachOfWorkoutAndVisibleToCoach().has_object_permission(test_request, None, ins)
        self.assertTrue(is_coach)

class IsPublicTestCase(TestCase):

    def setUp(self):
        #Initiate User, Workout, and RequestFactory
        self.user_one = User.objects.create(username = "jorsi", phone_number="12345678", country="Norway", 
        city="Trondheim", street_address="street 3", favourite_exercise="Biceps", main_gym="SIT")

        self.workout_one = Workout.objects.create(name="Chest", date="2022-03-03", notes="nice", 
        owner=self.user_one, visibility="PU")
      
        self.factory= RequestFactory()

    def test_has_object_permission(self):
        test_request = self.factory.get("/")
        test_request.user = self.user_one
        workout = self.workout_one

        is_visible = IsPublic().has_object_permission(test_request, None, workout)
        self.assertTrue(is_visible)

class IsWorkoutPublicTestCase(TestCase):

    def setUp(self):
        #Initiate User, Workout, Exercise, ExerciseInstance and RequestFactory
        self.user_one = User.objects.create(username = "jorsi", phone_number="12345678", country="Norway", 
        city="Trondheim", street_address="street 3", favourite_exercise="Biceps", main_gym="SIT")

        self.workout_one = Workout.objects.create(name="Chest", date="2022-03-03", notes="nice", 
        owner=self.user_one, visibility="PU")

        self.exercise_one = Exercise.objects.create(name="Pushups", description="push from ground", duration=30,
        calories=20, muscleGroup="Chest", unit="Amount")

        self.exercise_instance_one = ExerciseInstance.objects.create(workout=self.workout_one, exercise=self.exercise_one, 
        sets=3, number=10)

        self.factory= RequestFactory()

    def test_has_object_permission(self):
        test_request = self.factory.get("/")
        test_request.user = self.user_one
        ins = self.exercise_instance_one

        is_visible = IsWorkoutPublic().has_object_permission(test_request, None, ins)
        self.assertTrue(is_visible)

class IsReadOnlyTestCase(TestCase):

    def setUp(self):
        #Initiate User, Workout,  and RequestFactory
        self.user_one = User.objects.create(username = "jorsi", phone_number="12345678", country="Norway", 
        city="Trondheim", street_address="street 3", favourite_exercise="Biceps", main_gym="SIT")

        self.workout_one = Workout.objects.create(name="Chest", date="2022-03-03", notes="nice", 
        owner=self.user_one, visibility="PU")
        
        self.factory= RequestFactory()

    def test_has_object_permission(self):
        test_request = self.factory.get("/")
        test_request.user = self.user_one
        workout = self.workout_one

        is_read_only = IsReadOnly().has_object_permission(test_request, None, workout)
        self.assertTrue(is_read_only)