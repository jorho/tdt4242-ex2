from threading import local
from django.test import TestCase, Client


class TwoWayDomainTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.test_cases_data =[   
        #Valid
        { 
        "username" : "jorsi",
        "email" : "some@mail.com",
        "password" : "pw",
        "password1": "pw",
        "phone_number" : "12345678",
        "city" : "Ttown",
        "country":"Norwei",
        "street_address" : "street 3",
        "favourite_exercise" : "curl",
        "main_gym" :"SIT",
        },
        #Rest are invalid, first with several(username, email, password and password1) invalid inputs, then changing one by one
        {
             "username" : "//",
        "email" : "/&",
        "password" : "",
        "password1": "!",
        "phone_number" : "12345678",
        "city" : "Ttown",
        "country":"Norwei",
        "street_address" : "street 3",
        "favourite_exercise" : "curl",
        "main_gym" :"SIT",
        },
        {
        "username" : "//",
        "email" : "some@mail.com",
        "password" : "pw",
        "password1": "pw",
        "phone_number" : "12345678",
        "city" : "Ttown",
        "country":"Norwei",
        "street_address" : "street 3",
        "favourite_exercise" : "curl",
        "main_gym" :"SIT",
        },
        { 
        "username" : "jorsi",
        "email" : "/&",
        "password" : "pw",
        "password1": "pw",
        "phone_number" : "12345678",
        "city" : "Ttown",
        "country":"Norwei",
        "street_address" : "street 3",
        "favourite_exercise" : "curl",
        "main_gym" :"SIT",
        },
        { 
        "username" : "jorsi",
        "email" : "some@mail.com",
        "password" : "",
        "password1": "pw",
        "phone_number" : "12345678",
        "city" : "Ttown",
        "country":"Norwei",
        "street_address" : "street 3",
        "favourite_exercise" : "curl",
        "main_gym" :"SIT",
        },
        { 
        "username" : "jorsi",
        "email" : "some@mail.com",
        "password" : "pw",
        "password1": "",
        "phone_number" : "12345678",
        "city" : "Ttown",
        "country":"Norwei",
        "street_address" : "street 3",
        "favourite_exercise" : "curl",
        "main_gym" :"SIT",
        },
        ]

    def test_twoway_domain_test(self):

        for case in self.test_cases_data:
            response = self.client.post('/api/users/', case)
            if case == self.test_cases_data[0]:
                self.assertEqual(response.status_code, 201)
            else:
                self.assertEqual(response.status_code, 400)