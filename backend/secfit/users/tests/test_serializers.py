from django.test import TestCase
from users.serializers import UserSerializer
from rest_framework import serializers

# Create your tests here.

class UserSerializerTestCase(TestCase):

    def test_validate_password(self):
        my_pass = "pw"
        local_seri = UserSerializer(data={"password": my_pass, "password1": my_pass})
        self.assertEqual(local_seri.validate_password(my_pass), my_pass)

    def test_invalid_password(self):
        my_pass = "pw"
        invalid_pass = "hehe"

        local_seri = UserSerializer(data={"password": my_pass, "password1": invalid_pass})
        self.assertRaises(serializers.ValidationError, local_seri.validate_password, my_pass)

    def test_create(self):
        username = "jorsi"
        email = "some@mail.com"
        password = "pw"
        phone_number = "12345678"
        city ="Ttown"
        country="Norwei"
        street_address = "street 3"
        favourite_exercise = "curl"
        main_gym ="SIT"
        
        local_seri = UserSerializer(data={
        "username":username, 
        "email":email,
        "password":password,
        "phone_number":phone_number,
        "country":country,
        "city":city,
        "street_address":street_address,
        "favourite_exercise":favourite_exercise,
        "main_gym":main_gym
            })
        user_one=local_seri.create({
        "username":username, 
        "email":email,
        "password":password,
        "phone_number":phone_number,
        "country":country,
        "city":city,
        "street_address":street_address,
        "favourite_exercise":favourite_exercise,
        "main_gym":main_gym
        })
        #Can not check password to our variable, as the password is hashed.
        self.assertEqual(user_one.username, username)
        self.assertEqual(user_one.email, email)
        self.assertEqual(user_one.phone_number, phone_number)
        self.assertEqual(user_one.country, country)
        self.assertEqual(user_one.city, city)
        self.assertEqual(user_one.street_address, street_address)
        self.assertEqual(user_one.favourite_exercise, favourite_exercise)
        self.assertEqual(user_one.main_gym, main_gym)